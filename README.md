# Windows10環境構築(最終更新2018/04/18)

<!-- TOC -->

- [Windows10環境構築(最終更新2018/04/18)](#windows10環境構築最終更新20180418)
    - [注意書き](#注意書き)
    - [ディープラーニング環境構築](#ディープラーニング環境構築)
        - [Build Tools for Visual Studio 2017](#build-tools-for-visual-studio-2017)
        - [CUDA Toolkit 9.x](#cuda-toolkit-9x)
        - [cuDNN v7.x.x for CUDA 9.x](#cudnn-v7xx-for-cuda-9x)
        - [MSYS2 64bit](#msys2-64bit)
            - [起動バッチの種類によるプログラム実行結果の違い](#起動バッチの種類によるプログラム実行結果の違い)
        - [Anaconda 64bit Python 3.6 version](#anaconda-64bit-python-36-version)
        - [Visual Studio Code](#visual-studio-code)
            - [Visual Studio CodeでC/C++を書くための設定](#visual-studio-codeでccを書くための設定)
            - [Visual Studio CodeでPython(Anaconda 64bit)を書くための設定](#visual-studio-codeでpythonanaconda-64bitを書くための設定)
            - [Visual Studio Codeの見た目を変更する(おまけ)](#visual-studio-codeの見た目を変更するおまけ)
        - [Markdownでレポートを書く](#markdownでレポートを書く)
        - [freeglutのコンパイルオプション（おまけ）](#freeglutのコンパイルオプションおまけ)
        - [pandocのコンパイル(おまけ)](#pandocのコンパイルおまけ)

<!-- /TOC -->

## 注意書き

この文書はあくまでも、私のWindows10環境構築のための備忘録という扱いです。  
**`この文書に書いてある方法を試して発生した、いかなる問題やトラブルにつきましては一切の責任を負いかねますので、予めご了承下さい。`**  
**`また、この文書はNVIDIA製のGPUを搭載していることを前提とした環境構築手順となっています。`**  
**`他社製のGPUやオンボードグラフィックのPCを使用している方は対象外です。`**  
万が一、間違いや古い情報がございましたら、教えていただければ幸いです。  

ちなみに私は、Windows10 Home Insider Preview(Build 17063.1000)を使用していますが、通常のエディションでも同様の環境構築ができるかと思います。  

念の為、現時点(2018/04/16)での私のPCのスペックを記載しておきます。  

- OS : Windows10 Home 64bit Insider Preview(Build 17063.1000)
- CPU : Intel(R) Core(TM) i7-7700K CPU @ 4.20GHz 4.20GHz
- RAM : 24.0GB
- GPU : NVIDIA(R) GeForce GTX 1070 8GB GDDR5

## ディープラーニング環境構築

**`※必ず上から順にインストールして下さい。`**  
(CUDA ToolkitのインストールにはBuild Tools for Visual Studio 2017が必要だったり、cuDNNはそもそもCUDAのライブラリであったりする為)  

また基本的にこれらのツールやライブラリはデフォルトでCドライブ(OSがインストールされているドライブ)にインストールされますが、インストール先を聞かれた場合もCドライブにインストールするのがオススメです。  

### Build Tools for Visual Studio 2017

CUDAをインストールする前にVisual C++コンパイラをインストールする必要があります。  
現在はBuild Tools for Visual Studio 2017のインストーラーからVisual C++コンパイラをインストールすることができます。  

下記のサイトにアクセスし、ページ下部の**Build Tools for Visual Studio 2017**の右側にある**ダウンロード**というボタンをクリックします。  

> https://www.visualstudio.com/ja/downloads/?rr=http%3A%2F%2Flandinghub.visualstudio.com%2Fvisual-cpp-build-tools

![BuildTools_Download](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/BuildTools4VS2017.PNG)

**vs_buildtools__....exe**というファイルがダウンロードされるのでダブルクリックして実行します。  
しばらくすると、下のウィンドウが表示されるので、画面左上の**`Visual C++ Build Tools`**を選択します。  

![BuildToolsUI](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/BuildTools4VS2017UI.PNG)

この状態で、画面右側の概要欄のオプションの**`デスクトップ用の VC++ 2015.3 v140 ツールセット (x86、x64)`**にチェックを入れます。  
画面右下のインストールし、完了するまで待ちます。  

![BuildToolOverView](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/BuildTools4VS2017UI_Overview.PNG)

インストール完了後、**C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin**が存在すればインストールは成功しています。  
<br>
最後に環境変数のPATHに**C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin**を追加すればVisual C++ 2015 Build Toolsのセットアップは完了です。  

環境変数をWindowsに反映させるために一度再起動しましょう。  
**`(この操作をしないとCUDAのインストールでエラーが出ます)`**  

### CUDA Toolkit 9.x

ハードウェアとソフトウェアを有効にする テクノロジーとして、CUDA は１つのグラフィック プロセッサ内で多数のコンピューティング コアの使用を可能にすることで、演算速度を劇的に速めると同時に汎用数値計算の処理も可能としています。  
(NVIDIA公式サイト(http://www.nvidia.co.jp/object/cuda_whatis_jp.html)
より引用)  

**`TensorFlowを使う場合はCUDA9.0をインストールすることをお勧めします。使用するライブラリが対応しているCUDAのバージョンを確認してください。`**  
**`ChainerはCUAD9.1に対応確認済みです。`**  

万が一、CUDA9.1以前のバージョンをインストールする場合は、下記リンクの**Legacy Releases**というボタンをクリックし、バージョンを指定してください。  
![Legacy_Releases](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/CUDA_LegacyReleases.PNG)

ここでは、CUDA Toolkit9.1を例にインストール方法の説明をしますが、他のバージョンの場合でも同じやり方でインストールできます。  
<br><br>
下記のサイトにアクセスし、緑のWindowsボタンをクリックすると、画像のように展開されます。  
OSやVersion等は自身の使用しているものを利用して下さい。  
Installer Typeはexe[local]を選択しましょう。  
> https://developer.nvidia.com/cuda-downloads

![CUDA_Download](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/CUDA_Download_page.png)

**cuda_9.x.xx_win10.exe**がダウンロードされるのでダブルクリックで実行します。  
すると、このように一時的にCUDAのインストーラーを展開する場所を聞かれますが、そのままOKを押して続行します。  

![CUDA_Setup](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/cuda_setup_package.png)

システムチェックが入り次の画面に移ったら、同意して続行する(<u>A</u>)ボタンを押します。  
インストールオプションの画面で高速(推奨)(<u>E</u>)が選択されていることを確認し、次(<u>N</u>)
を押します。  
**CUDA Visual Studio Integration**の画面で色々書いていますが、気にせずI understand, and...にチェックを入れましょう。  
Nextボタンを押すとインストールが始まります。  

CUDA Toolkitもインストールに少し時間が掛かるので完了するまで待ちましょう。  
CUDA Toolkitのインストール手順については以上です。  

### cuDNN v7.x.x for CUDA 9.x

cuDNNはディープニューラルネットワーク用のGPUアクセラレーションライブラリです。  
簡単に説明すると、Deep Learning用のライブラリで、このライブラリを使用することで深いネットワークの学習速度を大幅に向上させることができます。  

cuDNNのダウンロードはNVIDIA Developerの登録が必要です。  
もし登録していなければ下記のサイトから登録しましょう。  
> https://developer.nvidia.com/

![NVIDIA_Developer_Register](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/nvidia_developer_register.png)

サイトにアクセスするとこのようなページが表示されるので、赤丸で囲った**Register Now**というボタンを押し、指示に従って登録を完了させましょう。  

登録が終われば、下記のサイトにアクセスし、**I Agree To the Terms of the cuDNN Software License Agreement**にチェックを入れ、先ほどインストールしたCUDAのバージョンに合うcuDNNをクリックします。  
**`(今回はWindows10にcuDNN v7.0.5 for CUDA9.1を導入する例で説明しますが、他のバージョンでも同様の方法で導入可能です。)`**  

> https://developer.nvidia.com/rdp/cudnn-download

![cuDNN_Download1](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/cudnn_download.png)

するとたくさんのバージョンのcuDNNが出てくるので、画像の赤丸で囲まれた箇所をクリックしてダウンロードします。  

![cuDNN_Download2](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/cudnn_download2.png)

ダウンロードしたものを展開し、**cuda**フォルダの中にある、**bin**、**include**、**lib**の3つのフォルダーをコピーします。  

![cuDNN_Download2](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/cudnn_copy.png)

コピーした3つのフォルダーを、CUDA Toolkitがインストールされている**C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.1**に貼り付けます。  
(この操作を実行するアクセス許可が必要です等のメッセージが表示されることがありますが、その場合は**はい**や**続行**などを押して操作を続けて下さい。)  

![cuDNN_Paste](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/cudnn_paste.png)

以上でcuDNNの導入は完了です。  

### MSYS2 64bit

MSYS2はMSYSの後継でありCygwinとMinGW-w64をベースとし、Windowsソフトウェアとより良い相互運用を目指して開発が行われています。  
ターミナルがmintty、シェルはbashなので、LINUXユーザーも戸惑う事なく利用できるのがメリットです。  
パッケージマネージャーはArch系で採用されているpacmanですが、利用したことがない人もすぐに慣れるかと思います。  

下記のサイトにアクセスし、**msys2-x86_64-20161025.exe**をクリックしてインストーラーをダウンロードします。  

> http://www.msys2.org/

インストーラーがダウンロードできたらダブルクリックで実行します。  
MSYS2 64bitのインストールはデフォルトのままで次へ(<u>N</u>)をクリックしていきます。  

インストールが完了すればターミナルが立ち上がりますが、一旦そのターミナルを閉じます。  
MSYS2は起動バッチが3種類あり、動作は似ていますが、セットされる環境変数が異なります。  

- mingw32.exe
- mingw64.exe
- msys2.exe

mingw32.exe、mingw64.exeはコンパイル環境にそれぞれmingw32、mingw64が提供されておりC++11から追加された**std::random_device**で固定の乱数列が生成されるという不具合(というか仕様?)があります。  
msys2.exeはコンパイル環境にgccが提供されているのでこういった問題は起こりません。  

試しにC++で下記のコードをmingw64とgccでコンパイルし実行してみました。  

```cpp
#include <iostream>
#include <random>
using namespace std;

int main(void){
    random_device rnd;
    for(int i = 0; i < 5; i++){
        cout << rnd() << endl;
    }
    return 0;
}
```

#### 起動バッチの種類によるプログラム実行結果の違い
左がmingw64、右がgccでコンパイルした実行結果です。  
このように、mingw64では何度実行しても同じ値が生成されていることが分かります。  

![mingw_gcc](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/random_mingw_gcc.png)

このような結果から私はmsys2.exeを使用することにします。  
C++の乱数生成やMSYS2の起動スクリプトについて詳しく知りたい方は下記のサイトを見てみると良いでしょう。  
> https://qiita.com/magichan/items/cdbb1b1e3795ad916367  
> https://cpprefjp.github.io/reference/random/random_device.html  

また、msys2.exeの起動バッチはMSYS2をデフォルトでインストールした場合、**C:\msys64\msys2.exe**
にあるので、タスクバーにピン留めしておくと便利です。  

それでは**C:\msys64\msys2.exe**を立ち上げ、パッケージのアップデートをしていきます。  
```
$ pacman -Syuu
```
このコマンドはインストールされているパッケージのアップデートという意味です。  

ちなみに詳しく説明すると、

- -Sy : パッケージデータベースの更新  
- -Su : パッケージの更新(アップデートのみ)  
- -Suu : パッケージの更新(ダウングレードも許可する)  

という意味になります。  

アップデート中にこのような警告が表示されることがありますが、その場合は一度ウィンドウを閉じ、再度ターミナルを立ち上げましょう。  

```
警告: terminate MSYS2 without returning to shell and check for updates again
警告: for example close your terminal window instead of calling exit
```

立ち上げ直したら、再び上と同じコマンドを実行します。  
警告がでなければ、パッケージのアップデートは完了です。  

初期状態では、必要最低限のパッケージしかインストールされていないので下記のコマンドを実行しておきましょう。  

```
$ pacman -S gcc git gcc-libs openssh winpty make
```

(pacmanコマンド周りに関しては私は詳しくないので下記のサイトを参考にしてみると良いかと思います。)  
> https://qiita.com/sira/items/f90bdd70b3243af159cc

次に、MSYS2はデフォルトではWindowsの環境変数PATHを読み込めないため、読み込めるように設定していきます。  
(この操作をしないと次に説明するAnacondaのPATHをMSYS2で見つけることができず実行することができません。)  

まず、コントロールパネルを開き、**システムとセキュリティ -> システム**の順にクリックします。  
するとこの画面が表示されるので、赤丸で囲っているシステムの詳細設定をクリックします。  

![ControlPanel](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/control_panel.png)

別ウィンドウが表示されるので、右下の環境変数(<u>N</u>)...をクリックします。  
そうすると、Windowsに設定されている環境変数の一覧が表示されるので赤丸で囲った**新規(N)...**
をクリックします。  

![AddPath](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/add_path.png)

そして、変数名(N): の箇所に**MSYS2_PATH_TYPE**、変数値(V): の箇所に**inherit**と記入して**OK**を押します。  

![AddVariable](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/add_variable.png)

これでMSYS2でWindowsの環境変数を読み込むことができるようになりました。  

### Anaconda 64bit Python 3.6 version

AnacondaはPythonとよく利用されるライブラリをセットで配布しているPythonパッケージです。  
AnacondaをインストールするだけでPython環境の構築ができます。  

Anaconda公式サイトにアクセルし、Anaconda Installerをダウンロードしましょう。   
> https://www.anaconda.com/download/

Python 3.6 versionとPython 2.7 versionがありますが、どちらにするかは自身の環境に合わせましょう。  
どちらが良いか分からない、また勉強目的でPythonを使用するという方はPython 3.6 versionで良いかと思います。  

Anacondaのインストーラーがダウンロードできたらダブルクリックで実行します。  
`基本的に Next > をクリックしていって問題はありませんが、インストール直前の`**`Advanced Options`**`で必ず`
**`Add Anaconda to my PATH environment variables`**`と`**`Register Anaconda as my default Python 3.6`**`にチェックを入れましょう。`  
これをしないとMSYS2からAnacondaを呼び出す時に面倒です。  

![AnacondaAddPATH](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/anaconda_setup.png)

最後に**Install**を押し、インストールが完了するまで待ちます。  

Anacondaのインストールが終了したら、MSYS2でPythonを立ち上げられるようにします。  

MSYS2のターミナルであるminttyはPythonを含む一部の対話モードを立ち上げられないという問題点があります。  
その問題点を解決するためにwinptyというラッパーを介してPythonを立ち上げます。  

MSYS2インストール時に先程紹介したコマンドを実行した方はすでにwinptyはインストールされた状態になっています。  
先程のコマンドを実行されていない方は下記のコマンドをMSYS2で実行して、winptyをインストールしてください。  

```
$ pacman -S winpty
```

winptyがインストールできたら下記のコマンドを順に実行し、winpty経由でPythonを立ち上げられるようにします。  

```
$ echo 'export ANACONDA_HOME=$HOMEDRIVE$HOMEPATH/Anaconda3' >> ~/.bash_profile
$ echo 'export PATH=$ANACONDA_HOME/Scripts:$ANACONDA_HOME:$PATH' >> ~/.bash_profile

$ echo 'alias python="winpty python"' >> ~/.bash_profile
$ echo 'alias ipython="winpty ipython"' >> ~/.bash_profile
$ echo 'alias jupyter="winpty jupyter"' >> ~/.bash_profile

$ source ~/.bash_profile
```

ここまでできたらMSYS2でpythonと打ち、Pythonが対話モードで立ち上がるか確認します。  

```
$ python
Python 3.6.3 |Anaconda, Inc.| (default, Oct 15 2017, 03:27:45) [MSC v.1900 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

このような表示がされていたらAnacondaのインストールは完了です。  
もし何も表示されないという場合はコマンドの実行のし忘れが無いか、タイプミスをしていないかを確認しましょう。  

### Visual Studio Code

Visual Studio CodeはMicrosoft社製のエディターであり、オープンソース、クロスプラットフォームで開発が進められています。  
エディターは他にもAtomやSublime Textなどがあるが、Atomは少し動作が重たかったりSublime Textは本来は有料のエディターであったりするので今回はこちらをインストールしていきます。  

Visual Studio Codeのダウンロードページにアクセスし、**Windows**ボタンをクリックしてダウンロードします。  
> https://code.visualstudio.com/download

インストーラーがダウンロードできたら、ダブルクリックで実行します。  
基本的にデフォルトのまま 次へ(N) > をクリックして問題ありません。  
インストールが終われば、Visual Studio Codeが立ち上がります。  

デフォルトの状態ではVisual Studio Codeのターミナルがbashになっていないのでbashに変更します。  
> https://qiita.com/yumetodo/items/42132a1e8435504448aa

を参考(というかMSYS以外コピペ)に以下の内容をVisual Studio Codeの設定に記述しました。  

```json:setting.json
{
    "terminal.integrated.shell.windows": "C:/msys64/usr/bin/bash.exe",
    "terminal.integrated.env.windows": {
        "MSYSTEM": "MSYS",
        "CHERE_INVOKING": 1
    },
    "terminal.integrated.shellArgs.windows": [
        "--login"
    ],
    "terminal.integrated.cursorStyle": "line"
}
```

この設定はMSYS2が立ち上がるようになっているので、もしMINGW64が立ち上がるようにしたい方は`MSYSTEM`の箇所を下記のようにしてください。  

```
"MSYSTEM": "MINGW64",
```

詳細に関しては、必ず上記サイトで確認するようにして下さい。  

#### Visual Studio CodeでC/C++を書くための設定

次に、C/C++を利用する方のみインクルードパスの指定も行う必要があります。  
Visual Studio CodeでC/C++のファイルを開くと、このようなC/C++の拡張機能のインストールを提案されますのでインストールします。  

![C_CPP](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/c_cpp.png)

インストールが終われば、一度Visual Studio Codeを再起動します。  
再起動後、Visual Studio Codeをアクティブウィンドウにしている状態で**F1**キーを押下します。  
すると画面上部にこのようなコンボボックスが表示されるので、そこに**C/Cpp: Edit Configurations**と入力します。  
これにより、ワークスペース(自身が開いている.cppがあるフォルダ)に.vscode/c_cpp_properties.jsonが作成されます。  

![ComboBox](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/combo_box.png)

次に、Visual Studio Codeの画面左側にある5のボタンの内、赤丸で囲ったボタンをクリックします。  

![VSCode_Button](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/vscode_button.jpg)

そうすると、今開いているファイルやフォルダーを見ることができ、そこに先程作成した.vscode/c_cpp_properties.jsonがあるはずなのでクリックして開きます。  

![C_CPP_Properties](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/c_cpp_properties.jpg)

すると、このような70~80行程の設定ファイルになっていますが、MinGW用の設定ではないため、全て削除します。  

```
{
    "configurations": [
        {
            "name": "Mac",
            "includePath": [
                "/usr/include",
                "/usr/local/include",
                "${workspaceRoot}"
            ],
            "defines": [],
            "intelliSenseMode": "clang-x64",
            "browse": {
                "path": [
                    "/usr/include",
                    "/usr/local/include",
                    "${workspaceRoot}"
                ],
                "limitSymbolsToIncludedHeaders": true,
                "databaseFilename": ""
            },
            "macFrameworkPath": [
                "/System/Library/Frameworks",
                "/Library/Frameworks"
            ]
        },

        .
        .
        .
```

全ての行を削除したら、次の設定をコピーして下さい。  

```
{
    "configurations": [
        {
            "name": "Win32",
            "intelliSenseMode": "clang-x64",
            "includePath": [
                "${workspaceRoot}",
                "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include/c++",
                "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include/c++/x86_64-pc-msys",
                "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include/c++/backward",
                "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include",
                "C:/msys64/usr/include",
                "C:/msys64/mingw64/include",
                "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include-fixed"
            ],
            "defines": [
                "_DEBUG",
                "UNICODE",
                "__GNUC__=6",
                "__cdecl=__attribute__((__cdecl__))"
            ],
            "browse": {
                "path": [
                    "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include",
                    "C:/msys64/usr/lib/gcc/x86_64-pc-msys/6.4.0/include-fixed",
                    "C:/msys64/mingw64/include/*",
                    "C:/msys64/usr/include/*"
                ],
                "limitSymbolsToIncludedHeaders": true,
                "databaseFilename": ""
            }
        }
    ],
    "version": 3
}
```

こちらはVisual Studio CodeをMSYS2環境で使用する際の設定のテンプレートになります。  
このサイトを参考に作成しました。  

> https://github.com/Microsoft/vscode-cpptools/blob/master/Documentation/LanguageServer/MinGW.md

以上でVisual Studio CodeでC/C++を書くための設定を終わります。  

#### Visual Studio CodeでPython(Anaconda 64bit)を書くための設定

次に、Visual Studio CodeでPython(先程インストールしたAnaconda環境のもの)を書くための設定について解説します。  
解説と言ってもVisual Studio CodeにAnacondaのパスを通すだけなので簡単に終わります。  

まず、Visual Studio Codeで.pyのファイルを開くか新規ファイルを作成して.py形式で保存します。  
そうすると、このようなPythonの拡張機能のインストールを提案されるので、インストールします。

![Python](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/python.png)

この拡張機能はPythonの文法誤りに下線を付ける機能や、モジュールの補完をおこなう機能があるため非常にスムーズにコーディング作業が出来るようになります。  

それではAnacondaのパスをVisual Studio Codeに記述しましょう。  
まず、ターミナル(msys2.exe)を立ち上げ、下記のコマンドを入力します。  

```
$ where python
```

これは、Pythonがどこにインストールされているのかを確認するためのコマンドです。  

私の環境ではこのような結果が返ってきました。  

```
C:\Users\yamagen\Anaconda3\python.exe
C:\msys64\mingw64\bin\python.exe
```

上が先程インストールしたAnaconda環境でのPythonのパス、下がmsys2でインストールした(もしくは他のパッケージインストール時に一緒にインストールされた)Pythonのパスです。  

今回はAnaconda環境のPythonを使用するので**C:\Users\yamagen\Anaconda3\python.exe**をコピーします。  
`(あくまでも私の場合です。ユーザー名が自身の名前になっていると思うので、このページの内容をそのままコピーしてもパスが反映されません。必ず`**`where python`**`を実行して確かめて下さい。)`  

Visual Studio Codeの上にあるメニューの[ファイル] - [基本設定] - [設定]をクリックしていくと、ユーザー設定という画面が開きます。  
ユーザー設定の中から、**Python Configuration**という項目を探します。(だいぶ下の方にあります)  

![PythonConfig](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/python_config.png)

**Python Configuration**をクリックすると**Python**の設定項目が展開されるので、その中からさらに"python.pythonPath": "python"という項目を探します。  

![PythonPATHCopy](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/python_path.jpg)

見つかったら、左側のペンのアイコンをクリックし、**設定にコピー**というボタンをクリックします。  
これで、画面右側のユーザー設定欄に設定がコピーされたので、先程調べたAnacondaのパスに書き換えます。  
私の場合、下記のようになります。  

**Before**  
![PythonPATHBefore](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/python_path_before.png)  

**After**  
![PythonPATHAfter](https://gitlab.com/VimYamagen/how2setup-Win10-Deep-Environment/raw/pics/python_path_after.png)  
`(Visual Studio Codeのユーザー設定にはバックスラッシュが含まれているとエラーが出るので、このようにスラッシュに置き換えてください。)`  

書き換えが終わったら保存し、Visual Studio Codeを再起動して設定完了です。  
次回起動時からは補完やLintなどが効くようになっていると思います。  

#### Visual Studio Codeの見た目を変更する(おまけ)

Visual Studio Codeは標準でたくさんの配色テーマが付属しており、メニューの[ファイル] - [基本設定] - [配色テーマ]から配色を変更することができます。  
ちなみに私のオススメは**Monokai**です。  

また、拡張機能でエディター画面の背景に画像を表示させることもできます。  
> https://marketplace.visualstudio.com/items?itemName=shalldie.background

この拡張機能は色々調べているとVisual Studio Codeの中の設定を無理矢理書き換えているそうなので
個人的にはあまりオススメしません。  
それでもインストールしたいという方は上記の配布サイトにアクセスし、**Install**をクリックしてインストールを行います。  
そして、メニューの[ファイル] - [基本設定] - [設定]をクリックし、ユーザー設定を開きます。  
画面右側のユーザー設定の箇所に以下の内容を追記します。  

```
"background.useDefault": false,
    "background.style": {
        "content": "''",
        "pointer-events": "none",
        "position": "absolute",
        "z-index": "99999",
        "width": "100%",
        "height": "100%",
        "background-position": "95% 95%",
        "background-repeat": "no-repeat",
        "background-size": "25%",
        "opacity": 0.25
    },
    "background.customImages": [
        "PATH_to_your_image_file"
    ]
```

この設定に関して簡単に表にまとめました。  
全ては載せていませんが、この辺りを設定するだけでも充分だと思います。  

| 変数 | 変数値 | 説明 |
| :------: | :------: | :------: |
| background.useDefault | True <br> false | デフォルトの画像を使用するか。falseにすると自分の好きな画像を使用することが出来る。 |
| width | 0-100% | 画像の幅を指定できる。 |
| height | 0-100% | 画像の高さを指定できる。 |
| background-position | 0-100% <br> 0-100% | 画像を配置するX座標 Y座標を指定することができる。 |
| background-size | 0-???% | 画像の拡大、縮小率を指定することができる。 |
| opacity | 0-1 | 画像の透過率を指定することが出来る。 |
| background.customImages | PATH | 表示したい画像のパスを記述する |

最後の**background.customImages**ですが、例えばピクチャフォルダーにあるsample.pngという画像を表示したい場合はこのようになります。  

```
    "background.customImages": [
        "C:/Users/UserName/Pictures/sample.png"
    ]
```

この拡張機能を有効にするにはVisual Studio Codeを管理者として実行する必要があります。  
Visual Studio Codeのプロパティを開き、互換性タブの**管理者としてこのプログラムを実行する**
にチェックを付けます。  

上でも書きましたが、この拡張機能はVisual Studio Code内部の設定を無理矢理書き換えているので [サポート対象外] という文字がウィンドウ上部に表示されるようになります。  

より詳しい説明は下記のサイトを参照して下さい。  
> https://qiita.com/TakYaz/items/9e06099ea738cf450c73

### Markdownでレポートを書く

私は大学でのレポートを書く時にLaTeXを使用せず、Markdownで書いています。  
理由はMarkdownの記法に私自身が慣れているというのもありますが、LaTeXに比べて記法が簡単です。  

今回はおまけとして、Markdownでレポートを書くための環境構築方法についても紹介します。  

まず、Markdownは.mdという拡張子であり、このままでは.texのようにレポートの形式にはなっていません。  
この.mdをpdfファイルや他の形式に変換してくれるツールがpandocです。  
さらにpandocのプラグインとして提供されているpandoc-crossrefも使用することでLaTeXと見た目がほとんど変わらないレポートを作成することができます。  

それではこれからpandocとpandoc-crossrefのインストールを行っていきます。  

まず、
> https://github.com/jgm/pandoc/releases

よりpandoc-x.x.x-windows.msi(バージョンはダウンロード時期により異なります)をダウンロードし、インストーラーの指示に従って進めていくだけでpandocのインストールは完了します。  

次に、pandoc-crossrefのインストール時に必要になるHaskell Platformのインストールを行います。  
> https://www.haskell.org/platform/

にアクセスし、**Download Core(64bit)**をクリックしてインストーラーをダウンロードします。  
こちらもインストーラーの指示に従って進めていけばインストールは完了します。  

ここまでできたら、MINGW64を立ち上げ、以下のコマンドを上から順に入力して下さい。  

```
cabal update
mkdir pandoc-crossref
cd pandoc-crossref
cabal sandbox init
cabal install pandoc-crossref
```

これらのコマンドを入力すると、pandoc-crossref/.cabal-sandbox/binにpandoc-crossrefがインストールされます。  
ただこの状態ではPATHが通っていないのでPATHを通します。  

~/workspace/にpandoc-crossrefディレクトリを作成し、pandoc-crossrefをインストールした場合の例になります。  
~/.bash_profileに以下を追記します。  

```
export PATH=$HOME/workspace/pandoc-crossref/.cabal-sandbox/bin:$PATH
```

これでpandocとpandoc-crossrefのインストールは終わりですが、pandocは.mdからpdfファイルを生成する際にtexliveが必要になります。  

> https://www.tug.org/texlive/

にアクセスし、**How to acquire Tex Live**の横にあるdownloadをクリックし、texliveインストーラーをダウンロードします。  
こちらもインストーラーの指示に従ってインストールを完了させて下さい。  
pandoc-crossrefと同様にtexliveもPATHを通します。  

Cドライブにtexlive2017をインストールした場合の例です。  
~/.bash_profileに以下を追記します。 
```
export PATH=/c/texlive/2017/bin/win32:$PATH
```

以上で、Markdownでレポートを書くための環境構築方法についての紹介を終わります。  
pandoc, pandoc-crossrefについては私自身分かっていない部分が多々あるので各自調べてみて下さい。  

### freeglutのコンパイルオプション（おまけ）

MinGW64環境でOpenGL(freeglut)をコンパイルする時に必要になるコンパイルオプションです。  

```
-lopengl32 -lfreeglut -lm
```

### pandocのコンパイル(おまけ)

```
pandoc test.md -o test.pdf -F pandoc-crossref --pdf-engine=lualatex --toc
```
--tocは生成されたpdfファイルの頭に目次を付与するオプションです。  


プログラミング環境構築編はこれで以上となります。  